const Validator = (method, data):string => {

    const regexAction = {
        'special': /[\W]/g,
        'quotes': /['\''&'\"']/g,
        'notnumbers': /[^\d]/g,
        'notletters': /[A-Za-z]/g,
        'numbercomma': /[^\d,]/g,
        'onlyNumbers': /\D/g
    }

    return data.replace(regexAction[method], '');

}

const CapexGeneratorCode = (fechaActual):string=>{

    const APL = 'AGI';
    const MOD = 'SC';
    const GNE = 'TI';
    const ANIO = fechaActual.toString().split('/')[2];
    const MES = fechaActual.toString().split('/')[0];

    const  codigoAuto = `${APL}-${MOD}-${GNE}-${ANIO}-${MES}`;

    return codigoAuto.toString();
}

export {Validator,CapexGeneratorCode}