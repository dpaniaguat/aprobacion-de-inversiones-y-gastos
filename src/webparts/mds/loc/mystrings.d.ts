declare interface IMdsWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'MdsWebPartStrings' {
  const strings: IMdsWebPartStrings;
  export = strings;
}
