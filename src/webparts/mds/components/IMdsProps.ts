export interface IMdsProps {
  checked: { color: string; };
  description: string;
  multiValueOptions: any;
  singleValueOptions: any;
}
