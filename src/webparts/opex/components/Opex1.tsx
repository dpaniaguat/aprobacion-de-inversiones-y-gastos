import * as React from 'react';
import styles from './Opex.module.scss';
import { IOpexProps } from './IOpexProps';
import { escape } from '@microsoft/sp-lodash-subset';

import { Guid } from '@microsoft/sp-core-library';
import { sp, Web, IWeb } from "@pnp/sp/presets/all";
import "@pnp/sp/lists";
import "@pnp/sp/items";
import { Dropdown, PrimaryButton, IDropdownOption, TextField, Label, DefaultButton, Link, Panel, Icon, Checkbox, ICheckboxStyles, ICheckboxStyleProps, IButtonStyles, ChoiceGroup, IChoiceGroupOption, DetailsList, DetailsListLayoutMode, SelectionMode, IColumn, mergeStyleSets } from 'office-ui-fabric-react';

var arr = [];
export interface IDropdownStates {
  singleValueDropdown:string;
  multiValueDropdown:any;
}

export default class Opex extends React.Component<IOpexProps, IDropdownStates> {

  constructor(props) {
    super(props);

    this.state = {
      singleValueDropdown:"",
      multiValueDropdown:[],
    };

  }
  
  public onDropdownChange = (event: React.FormEvent<HTMLDivElement>, item: IDropdownOption): void => {
    this.setState({ singleValueDropdown: item.key as string});
  }

  public onDropdownMultiChange = async (event: React.FormEvent<HTMLDivElement>, item: IDropdownOption): Promise<void> => {
    if (item.selected) {
      arr.push(item.key as string);
    }
    else {
      arr.indexOf(item.key) !== -1 && arr.splice(arr.indexOf(item.key), 1);
    }
    this.setState({ multiValueDropdown: arr });
  }


  public render(): React.ReactElement<IOpexProps> {

    const options: IChoiceGroupOption[] = [
      { key: '1', text: 'Yes' },
      { key: '2', text: 'No' }
    ];
    const optionsMoneda: IChoiceGroupOption[] = [
      { key: '1', text: 'USD' },
      { key: '2', text: 'PEN' }
    ];
    

    return (
      <div className={ styles.opex }>
        <div className={ styles.container }>
          <div className={styles.box}>
            <div className={styles.header}>
              <h3>General Information</h3>
            </div>
            <div className={styles.content}>
              <div className={styles.msGrid}>
                <div className={styles.msGridrow}> 
                  <div className={styles.msGridcol2}>
                    <label className={styles.head}>Request:</label>
                    <label className={styles.data}>Erik Suarez</label>
                  </div>
                  <div className={styles.msGridcol2}>
                    <label className={styles.head}>Area:</label>
                    <label className={styles.data}>General Manager</label>
                  </div>
                  <div className={styles.msGridcol2}>
                    <label className={styles.head}>Date of inssuance:</label>
                    <label className={styles.data}>15/09/21</label>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className={styles.box}>
            <div className={styles.header}>
              <h3>Management Decision Sheet</h3>
            </div>
            <div className={styles.content}>
              <div className={styles.msGrid}>
                <div className={styles.msGridrow}> 
                  <div className={styles.msGridcol4}>
                    <label className={styles.head}>Type:</label>
                    <div className={styles.msGridcol5}>
                      <div className={styles.checkbox}><Checkbox label="Not budgeted" defaultChecked/></div>
                    </div>
                    <div className={styles.msGridcol5}>
                      <div className={styles.checkbox}><Checkbox label="Exceeds budget" /></div>
                    </div>
                  </div>

                  <div className={styles.msGridcol8}>
                  <label className={styles.info}><Icon iconName={"Info"} /><a href="#">See approval scales</a></label>
                  </div>                                    
                </div>

                <div className={styles.msGridrow}> 
                  <div className={styles.msGridcol2}>
                    <div className={styles.textfield}><TextField label="CECO Code:" /></div>
                  </div>
                  <div className={styles.msGridcol4}>
                    <div className={styles.textfield}><TextField label="Name Acquisition/contracting:" /></div>
                  </div>   
                  <div className={styles.msGridcol2}>
                    <div className={styles.textfield}><TextField label="Budget Item Code:" /></div>
                  </div> 
                  <div className={styles.msGridcol4}>
                    <div className={styles.textfield}><TextField label="Budget Item Name:" /></div>
                  </div> 
                </div> 

                <div className={styles.msGridrow}> 
                  <div className={styles.msGridcol2}>
                    <div className={styles.choice}>
                      <label className={styles.head}>Currency:</label>
                      <ChoiceGroup defaultSelectedKey="1" options={optionsMoneda} onChange={_onChange} required={false} />
                    </div>
                  </div>
                  <div className={styles.msGridcol2}>
                    <div className={styles.choice}>
                      <label className={styles.head}>Older than one year?</label>
                      <ChoiceGroup defaultSelectedKey="1" options={options} onChange={_onChange} required={false} />
                    </div>
                  </div>                   
                </div> 

                <div className={styles.msGridrow}> 
                  <div className={styles.msGridcol2}>
                    <div className={styles.textfield}><TextField label="Initial Budget (PEN):" /></div>
                  </div>
                  <div className={styles.msGridcol2}>
                    <div className={styles.textfield}><TextField label="Additional Budget (PEN):" /></div>
                  </div>   
                  <div className={styles.msGridcol2}>
                    <div className={styles.textfield}><TextField label="Total value (PEN):" /></div>
                  </div> 
                </div>                

              </div>
            </div>
          </div>

          <div className={styles.box}>
            <div className={styles.header}>
              <h3>Justification (Explain the reasons why the budget has been exceeded)</h3>
            </div>
            <div className={styles.content}>
              <div className={styles.msGrid}>

                <div className={styles.msGridrow}> 
                  <div className={styles.msGridcol12}>
                    <div className={styles.textfield}><TextField multiline rows={3} /></div>
                  </div>
                </div>
                <div className={styles.msGridrow}>
                  <div className={styles.msGridcol3}>
                    <div className={styles.msGridrow}>
                      <div className={styles.msGridcol12}>
                        <label className={styles.head}>Variation rate:</label>
                        <div className={styles.checkbox}>
                          <Checkbox label="Scope" defaultChecked/>
                          <Checkbox label="Cost" />
                          <Checkbox label="Time" />
                          <Checkbox label="Quality" />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className={styles.msGridcol9}>
                    <div className={styles.msGridrow}>
                      <div className={styles.msGridcol9}>
                        <label className={styles.head}>Attachments</label>
                      </div>
                      <div className={styles.msGridcol3}>
                        <label className={styles.adjunto}><Icon iconName={"FabricOpenFolderHorizontal"} /><a href="#">Open attachments folder</a></label>
                      </div>
                      <div className={styles.msGridcol12}>

                      tabla

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className={styles.box}>
            <div className={styles.header}>
              <h3>Funding Source (Explain where the funding comes from)</h3>
            </div>
            <div className={styles.content}>
              <div className={styles.msGrid}>

                <div className={styles.msGridrow}> 
                  <div className={styles.msGridcol12}>
                    <DefaultButton text="Add Funding Source" className={styles.btn_blue} />
                  </div>
                </div>
                <div className={styles.msGridrow}>
                  <div className={styles.msGridcol12}>

                  tabla

                  </div>
                  <div className={styles.msGridcol12}>
                    <label className={styles.label}>(<span>*</span>) Reasons why a budgeted activity has been dropped (if it is no longer needed or can be postponed to the following year)</label>
                  </div>

                </div>
              </div>
            </div>
          </div>          

          <div className={styles.msGrid}>
            <div className={styles.msGridrow}>
              <div className={styles.msGridcol12}>
                <DefaultButton text="Cancel" className={styles.btn_red} />
                <DefaultButton text="Send" className={styles.btn_blue} />
                <DefaultButton text="Save draft" className={styles.btn_blue} />
              </div>
              <div className={styles.msGridcol12}>
                <div className={styles.separador}></div>
              </div>              
            </div>
          </div>

        </div>
      </div>
    );

  }
}

function _onChange(ev: React.FormEvent<HTMLInputElement>, option: IChoiceGroupOption): void {
  console.dir(option);
}

