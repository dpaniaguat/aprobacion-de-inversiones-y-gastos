export interface IOpexProps {
  checked: { color: string; };
  description: string;
  multiValueOptions: any;
  singleValueOptions: any;
}
