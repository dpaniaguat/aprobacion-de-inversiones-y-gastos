import * as React from 'react';
import styles from './Opex.module.scss';
import { IOpexProps } from './IOpexProps';
import { escape } from '@microsoft/sp-lodash-subset';

import { Guid } from '@microsoft/sp-core-library';
import { sp, Web, IWeb } from "@pnp/sp/presets/all";
import "@pnp/sp/lists";
import "@pnp/sp/items";
import { Dropdown, PrimaryButton, IDropdownOption, TextField, Label, DefaultButton, Link, Panel, Icon, Checkbox, ICheckboxStyles, ICheckboxStyleProps, IButtonStyles, ChoiceGroup, IChoiceGroupOption, DetailsList, DetailsListLayoutMode, SelectionMode, IColumn, mergeStyleSets } from 'office-ui-fabric-react';

var arr = [];
export interface IDropdownStates {
  singleValueDropdown:string;
  multiValueDropdown:any;
}

export default class Opex extends React.Component<IOpexProps, IDropdownStates> {

  constructor(props) {
    super(props);

    this.state = {
      singleValueDropdown:"",
      multiValueDropdown:[],
    };

  }
  
  public onDropdownChange = (event: React.FormEvent<HTMLDivElement>, item: IDropdownOption): void => {
    this.setState({ singleValueDropdown: item.key as string});
  }

  public onDropdownMultiChange = async (event: React.FormEvent<HTMLDivElement>, item: IDropdownOption): Promise<void> => {
    if (item.selected) {
      arr.push(item.key as string);
    }
    else {
      arr.indexOf(item.key) !== -1 && arr.splice(arr.indexOf(item.key), 1);
    }
    this.setState({ multiValueDropdown: arr });
  }


  public render(): React.ReactElement<IOpexProps> {

    const options: IChoiceGroupOption[] = [
      { key: '1', text: 'Yes' },
      { key: '2', text: 'No' }
    ];
    const optionsMoneda: IChoiceGroupOption[] = [
      { key: '1', text: 'USD' },
      { key: '2', text: 'PEN' }
    ];
    

    return (
      <div className={ styles.opex }>
        <div className={ styles.container }>
          <div className={styles.box}>
            <div className={styles.header}>
              <h3>General Information</h3>
            </div>
            <div className={styles.content}>
              <div className={styles.msGrid}>
                <div className={styles.msGridrow}> 
                  <div className={styles.msGridcol2}>
                    <label className={styles.head}>Request:</label>
                    <label className={styles.data}>Erik Suarez</label>
                  </div>
                  <div className={styles.msGridcol2}>
                    <label className={styles.head}>Area:</label>
                    <label className={styles.data}>General Manager</label>
                  </div>
                  <div className={styles.msGridcol2}>
                    <label className={styles.head}>Date of inssuance:</label>
                    <label className={styles.data}>15/09/21</label>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className={styles.box}>
            <div className={styles.header}>
              <h3>Request Information</h3>
            </div>
            <div className={styles.content}>
              <div className={styles.msGrid}>
                <div className={styles.msGridrow}> 
                  <div className={styles.msGridcol4}>
                    <label className={styles.head}>Type:</label>
                    <div className={styles.msGridcol5}>
                      <div className={styles.checkbox}><Checkbox label="Not budgeted" defaultChecked/></div>
                    </div>
                    <div className={styles.msGridcol5}>
                      <div className={styles.checkbox}><Checkbox label="Exceeds budget" /></div>
                    </div>
                  </div>

                  <div className={styles.msGridcol8}>
                  <label className={styles.info}><Icon iconName={"Info"} /><a href="#">Open attachments folder</a></label>
                  </div>                                    
                </div>

                <div className={styles.msGridrow}> 
                  <div className={styles.msGridcol2}>
                    <label className={styles.head}>CECO Code:</label>
                    <label className={styles.data}>CC-0001</label>
                  </div>
                  <div className={styles.msGridcol4}>
                    <label className={styles.head}>Name Acquisition/contracting:</label>
                    <label className={styles.data}>Lorem ipsum dolor sit amet</label>
                  </div>   
                  <div className={styles.msGridcol2}>
                    <label className={styles.head}>Budget Item Code:</label>
                    <label className={styles.data}>P0001</label>
                  </div> 
                  <div className={styles.msGridcol4}>
                    <label className={styles.head}>Budget Item Name:</label>
                    <label className={styles.data}>Lorem ipsum dolor sit amet</label>
                  </div> 
                </div> 

                <div className={styles.msGridrow}> 
                  <div className={styles.msGridcol2}>
                    <div className={styles.choice}>
                      <label className={styles.head}>Currency:</label>
                      <ChoiceGroup defaultSelectedKey="1" options={optionsMoneda} onChange={_onChange} required={false} />
                    </div>
                  </div>
                  <div className={styles.msGridcol2}>
                    <div className={styles.choice}>
                      <label className={styles.head}>Older than one year?</label>
                      <ChoiceGroup defaultSelectedKey="1" options={options} onChange={_onChange} required={false} />
                    </div>
                  </div>                   
                </div> 

                <div className={styles.msGridrow}> 
                  <div className={styles.msGridcol2}>
                    <label className={styles.head}>Initial Budget (PEN):</label>
                    <label className={styles.data}>40,000.00</label>
                  </div>
                  <div className={styles.msGridcol2}>
                    <label className={styles.head}>Exchange rate Budget:</label>
                    <label className={styles.data}>4.0</label>
                  </div>   
                  <div className={styles.msGridcol2}>
                    <label className={styles.head}>Initial Budget (USD):</label>
                    <label className={styles.data}>10,000.00</label>
                  </div>   
                  <div className={styles.msGridcol2}>
                    <label className={styles.head}>Additional Budget (PEN):</label>
                    <label className={styles.data}>4,000.00</label>
                  </div> 
                  <div className={styles.msGridcol2}>
                    <label className={styles.head}>Exchange rate:</label>
                    <label className={styles.data}>4.0</label>
                  </div>
                  <div className={styles.msGridcol2}>
                    <label className={styles.head}>Additional Budget (USD):</label>
                    <label className={styles.data}>1,000.00</label>
                  </div> 
                </div>    

                <div className={styles.msGridrow}> 
                  <div className={styles.msGridcol2}>
                    <label className={styles.head}>Total value (USD):</label>
                    <label className={styles.data}>11,000.00</label>
                  </div> 
                </div>

              </div>
            </div>
          </div>

          <div className={styles.box}>
            <div className={styles.header}>
              <h3>Justification (Explain the reasons why the budget has been exceeded)</h3>
            </div>
            <div className={styles.content}>
              <div className={styles.msGrid}>

                <div className={styles.msGridrow}> 
                  <div className={styles.msGridcol12}>
                    <div className={styles.textfield}><TextField multiline rows={3} /></div>
                  </div>
                </div>
                <div className={styles.msGridrow}>
                  <div className={styles.msGridcol3}>
                    <div className={styles.msGridrow}>
                      <div className={styles.msGridcol12}>
                        <label className={styles.head}>Variation rate:</label>
                        <div className={styles.checkbox}>
                          <Checkbox label="Scope" defaultChecked/>
                          <Checkbox label="Cost" />
                          <Checkbox label="Time" />
                          <Checkbox label="Quality" />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className={styles.msGridcol9}>
                    <div className={styles.msGridrow}>
                      <div className={styles.msGridcol9}>
                        <label className={styles.head}>Attachments</label>
                      </div>
                      <div className={styles.msGridcol12}>

                      tabla

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className={styles.box}>
            <div className={styles.header}>
              <h3>Funding Source (Explain where the funding comes from)</h3>
            </div>
            <div className={styles.content}>
              <div className={styles.msGrid}>

                <div className={styles.msGridrow}>
                  <div className={styles.msGridcol12}>
                  tabla
                  </div>
                  <div className={styles.msGridcol12}>
                    <label className={styles.label}>(<span>*</span>) Reasons why a budgeted activity has been dropped (if it is no longer needed or can be postponed to the following year)</label>
                  </div>

                </div>
                <div className={styles.msGridrow}> 
                  <div className={styles.msGridcol12}>
                    <div className={styles.textfield}><TextField label="Comment:" multiline rows={3} /></div>
                  </div>
                </div>


              </div>
            </div>
          </div>          

          <div className={styles.msGrid}>
            <div className={styles.msGridrow}>
              <div className={styles.msGridcol12}>
                <DefaultButton text="Cancel" className={styles.btn_red} />
                <DefaultButton text="Rejected" className={styles.btn_blue} />
                <DefaultButton text="Observed" className={styles.btn_blue} />
                <DefaultButton text="Approved" className={styles.btn_blue} />
              </div>
              <div className={styles.msGridcol12}>
                <div className={styles.separador}></div>
              </div>              
            </div>
          </div>

        </div>
      </div>
    );

  }
}

function _onChange(ev: React.FormEvent<HTMLInputElement>, option: IChoiceGroupOption): void {
  console.dir(option);
}

