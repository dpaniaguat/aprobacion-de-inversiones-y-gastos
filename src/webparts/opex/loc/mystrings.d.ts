declare interface IOpexWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'OpexWebPartStrings' {
  const strings: IOpexWebPartStrings;
  export = strings;
}
