declare interface ICapexWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'CapexWebPartStrings' {
  const strings: ICapexWebPartStrings;
  export = strings;
}
