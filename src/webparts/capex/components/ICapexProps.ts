export interface ICapexProps {
  Requested: string;
  Area: string;
  pepCode: string;
  //checked: { color: string; };
  description: string;
  multiValueOptions: any;
  singleValueOptions: any;
  capexRequest: string;
  projectName: string;
  initialBudget: number;
  aditionalBudget: number;
  totalProjectValue: number;
  showInputs: boolean;
  budget_nobudget: boolean;
  budget_budgetexceded: boolean;
  olderThanOneYear: boolean;
  Justification: string;
  variationRate: object;
  Attachment: object;
}
