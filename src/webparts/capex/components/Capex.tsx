//import * as React from 'react';
import React, { ChangeEvent, useEffect } from 'react';
import styles from './Capex.module.scss';
import { ICapexProps } from './ICapexProps';
import { escape } from '@microsoft/sp-lodash-subset';

import { Guid } from '@microsoft/sp-core-library';
import { sp, Web, IWeb } from "@pnp/sp/presets/all";
import "@pnp/sp/lists";
import "@pnp/sp/items";
import { Dropdown, PrimaryButton, IDropdownOption, TextField, Label, DefaultButton, Link, Panel, Icon, Checkbox, ICheckboxStyles, ICheckboxStyleProps, IButtonStyles, ChoiceGroup, IChoiceGroupOption, DetailsList, DetailsListLayoutMode, SelectionMode, IColumn, mergeStyleSets } from 'office-ui-fabric-react';
import * as moment from 'moment'
import { CapexGeneratorCode, Validator } from '../../../utils/utils';

var arr = [];
export interface IFormCapexProps {
  Requested: string;
  Area: string;
  pepCode: string;
  description: string;
  singleValueDropdown: string;
  multiValueDropdown: any;
  multiValueOptions: any;
  singleValueOptions: any;
  capexRequest: string;
  projectName: string;
  initialBudget: number;
  aditionalBudget: number;
  totalProjectValue: number;
  showInputs: boolean;
  budget_nobudget: boolean;
  budget_budgetexceded: boolean;
  olderThanOneYear: boolean;
  Justification: string;
  variationRate: object;
  Attachment: object;
  FundingSource: any;
  isDisableBtn: boolean;
}

const variationRate = ['Scope', 'Cost', 'Time', 'Quality'];
const currentDay = moment().format('L');

export default class Capex extends React.Component<ICapexProps, IFormCapexProps> {

  constructor(props) {

    super(props);

    this.state = {
      Requested: 'Erik Suarez', //getCurrentUser
      Area: 'General Manager', //getCurrentArea
      pepCode: "",
      description: '',
      singleValueDropdown: "",
      multiValueDropdown: [],
      multiValueOptions: '',
      singleValueOptions: [],
      capexRequest: CapexGeneratorCode(currentDay),
      projectName: '',
      initialBudget: 0,
      aditionalBudget: 0,
      totalProjectValue: 0,
      showInputs: false,
      budget_nobudget: true,
      budget_budgetexceded: false,
      olderThanOneYear: true,
      Justification: '',
      variationRate: {},
      Attachment: {},
      FundingSource: [],
      isDisableBtn: true
    };

  }

  public onDropdownChange = (event: React.FormEvent<HTMLDivElement>, item: IDropdownOption): void => {
    this.setState({ singleValueDropdown: item.key as string });
  }

  public onDropdownMultiChange = async (event: React.FormEvent<HTMLDivElement>, item: IDropdownOption): Promise<void> => {
    if (item.selected) {
      arr.push(item.key as string);
    }
    else {
      arr.indexOf(item.key) !== -1 && arr.splice(arr.indexOf(item.key), 1);
    }
    this.setState({ multiValueDropdown: arr });
  }

  public render(): React.ReactElement<ICapexProps> {

    const options: IChoiceGroupOption[] = [
      { key: '1', text: 'Yes' },
      { key: '2', text: 'No' }
    ];

    const handleInputChange = ({ target }: ChangeEvent<HTMLInputElement>) => {

      const { name, value } = target;

      if (name.toString().indexOf('budget_') === 0) {

        if (name.toString() === 'budget_nobudget') {

          this.setState({
            ...this.state,
            budget_nobudget: true,
            budget_budgetexceded: false,
            showInputs: false
          });
        }

        if (name.toString() === 'budget_budgetexceded') {

          this.setState({
            ...this.state,
            budget_nobudget: false,
            budget_budgetexceded: true,
            showInputs: true
          });
        }

      } else {
        this.setState({ ...this.state, [name]: value });
      }

    };

    const handleAddSource = () => {

      const { pepCode, projectName, Area, totalProjectValue, Justification, variationRate, budget_nobudget, Attachment } = this.state;

      this.setState({
        FundingSource: [...this.state.FundingSource, {
          pepCode, projectName, Area, totalProjectValue, Justification, variationRate, budget_nobudget, Attachment
        }]
      });

    };

    const handleVariationRate = ({ target }: ChangeEvent<HTMLInputElement>) => {

      const { name, value, id } = target;
      const elem = document.querySelector(`#${id}`);
      const isChecked = elem.getAttribute('aria-checked');

      this.setState({
        variationRate: {
          ...this.state.variationRate,
          [name]: isChecked
        }
      });

    };

    const handleResetForm = () => {

    }

    const handleSend = () => {

    }

    const handleSaveDraft = async () => {

      const result = await sp.web.lists.getByTitle("SimpleCAPEX").append.arguments(this.state.FundingSource);

    }

    return (
      <div className={styles.capex}>
        <div className={styles.container}>
          <div className={styles.box}>
            <div className={styles.header}>
              <h3>General Information</h3>
            </div>
            <div className={styles.content}>
              <div className={styles.msGrid}>
                <div className={styles.msGridrow}>
                  <div className={styles.msGridcol2}>
                    <label className={styles.head}>Request:</label>
                    <label className={styles.data}>{this.state.Requested}</label>
                  </div>
                  <div className={styles.msGridcol2}>
                    <label className={styles.head}>Area:</label>
                    <label className={styles.data}>{this.state.Area}</label>
                  </div>
                  <div className={styles.msGridcol2}>
                    <label className={styles.head}>Date of inssuance:</label>
                    <label className={styles.data}>{currentDay}</label>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className={styles.box}>
            <div className={styles.header}>
              <h3>Request Information</h3>
            </div>
            <div className={styles.content}>
              <div className={styles.msGrid}>
                <div className={styles.msGridrow}>
                  <div className={styles.msGridcol4}>
                    <label className={styles.head}>Type:</label>
                    <div className={styles.msGridcol5}>
                      <div className={styles.checkbox}>
                        <Checkbox
                          label="Not budgeted"
                          checked={this.state.budget_nobudget}
                          name="budget_nobudget"
                          onChange={handleInputChange}
                        />
                      </div>
                    </div>
                    <div className={styles.msGridcol5}>
                      <div className={styles.checkbox}>
                        <Checkbox
                          label="Exceeds budget"
                          checked={this.state.budget_budgetexceded}
                          name="budget_budgetexceded"
                          onChange={handleInputChange}
                        />
                      </div>
                    </div>
                  </div>
                  <div className={styles.msGridcol8}>
                    <label className={styles.info}><Icon iconName={"Info"} /><a href="#">See approval scales</a></label>
                  </div>
                </div>

                <div className={styles.msGridrow}>

                  {
                    this.state.showInputs && (
                      <div className={styles.textfield}>
                        <TextField label="PEP Code:"
                          name='pepCode'
                          onChange={handleInputChange}
                          value={this.state.pepCode}
                        /></div>

                    ) || (
                      <div className={styles.msGridcol4}>
                        <div className={styles.textfield}><TextField label="CAPEX Request:" value={this.state.capexRequest} /></div>
                      </div>
                    )
                  }

                  <div className={styles.msGridcol4}>
                    <div className={styles.textfield}>
                      <TextField
                        label="Project name:"
                        name='projectName'
                        onChange={handleInputChange}
                        value={this.state.projectName}
                      />
                    </div>
                  </div>

                  {
                    this.state.showInputs && (
                      <>
                        <div className={styles.msGridcol4}>
                          <div className={styles.textfield}>
                            <TextField
                              label="Initial Budget (USD):"
                              value={Validator('numbercomma', this.state.initialBudget.toString())}
                              name="initialBudget"
                              onChange={handleInputChange}
                            />
                          </div>
                        </div>

                        <div className={styles.msGridcol4}>
                          <div className={styles.textfield}>
                            <TextField
                              label="Aditional Budget (USD):"
                              value={Validator('numbercomma', this.state.aditionalBudget.toString())}
                              name="aditionalBudget"
                              onChange={handleInputChange}
                            />
                          </div>
                        </div>
                      </>
                    )
                  }

                  {
                    this.state.showInputs && (

                      <div className={styles.msGridcol2}>
                        <div className={styles.textfield}>
                          <TextField
                            label="Total Project value (USD):"
                            name="totalProjectValue"
                            value={((parseFloat(this.state.initialBudget.toString()) +
                              parseFloat(this.state.aditionalBudget.toString()))).toString()}

                          />
                        </div>
                      </div>
                    ) || (

                      <div className={styles.msGridcol2}>
                        <div className={styles.textfield}>
                          <TextField
                            label="Total Project value (USD):"
                            name="totalProjectValue"
                            value={Validator('numbercomma', this.state.totalProjectValue.toString())}
                            onChange={handleInputChange}
                          />
                        </div>
                      </div>
                    )
                  }

                  <div className={styles.msGridcol2}>
                    <div className={styles.choice}>
                      <label className={styles.head}>Older than one year?</label>
                      <ChoiceGroup defaultSelectedKey="1" options={options} onChange={_onChange} required={false} />
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>

          <div className={styles.box}>
            <div className={styles.header}>
              <h3>Justification (Explain the reasons why the procurement/contracting is necessary and the years in which the differential amount is required)</h3>
            </div>
            <div className={styles.content}>
              <div className={styles.msGrid}>

                <div className={styles.msGridrow}>
                  <div className={styles.msGridcol12}>
                    <div className={styles.textfield}>
                      <TextField
                        multiline rows={3}
                        onChange={handleInputChange}
                        value={this.state.Justification}
                        name="Justification"
                      />
                    </div>
                  </div>
                </div>
                <div className={styles.msGridrow}>
                  <div className={styles.msGridcol3}>
                    <div className={styles.msGridrow}>
                      <div className={styles.msGridcol12}>
                        <label className={styles.head}>Variation rate:</label>
                        <div className={styles.checkbox}>
                          {
                            variationRate && (
                              variationRate.map((valor: string, index: number) => (
                                <Checkbox key={index} name={valor} label={valor} onChange={handleVariationRate} />
                              )
                              )
                            )
                          }
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className={styles.msGridcol9}>
                    <div className={styles.msGridrow}>
                      <div className={styles.msGridcol9}>
                        <label className={styles.head}>Attachments</label>
                      </div>
                      <div className={styles.msGridcol3}>
                        <label className={styles.adjunto} htmlFor="card-input-file">
                          <input type="file" id="card-input-file" multiple />
                          <Icon iconName={"FabricOpenFolderHorizontal"} />
                          Open attachments folder

                        </label>
                      </div>
                      <div className={styles.msGridcol12}>
                        Tabla
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className={styles.box}>
            <div className={styles.header}>
              <h3>Funding Source (Explain where the funding comes from)</h3>
            </div>
            <div className={styles.content}>
              <div className={styles.msGrid}>

                <div className={styles.msGridrow}>
                  <div className={styles.msGridcol12}>
                    <DefaultButton text="Add Funding Source" className={styles.btn_blue} onClick={handleAddSource} />
                  </div>
                </div>
                <div className={styles.msGridrow}>
                  <div className={styles.msGridcol12}>
                    <table className={styles.tablaSource}>
                      <thead>
                        <th>PEP Code</th>
                        <th>Project Name</th>
                        <th>Area</th>
                        <th>Copy to</th>
                        <th>Amount(USD)</th>
                        <th>Justification(*)</th>
                      </thead>
                      <tbody>
                        {
                          this.state.FundingSource && (
                            this.state.FundingSource.map(({ pepCode, projectName, Area, totalProjectValue, Justification, variationRate, budget_nobudget, Attachment }, key) => (
                              <tr key={key}>
                                <td >{pepCode}</td>
                                <td>{projectName}</td>
                                <td>{Area}</td>
                                <td>--</td>
                                <td>{totalProjectValue}</td>
                                <td>{Justification}</td>
                              </tr>
                            ))
                          )
                        }
                      </tbody>
                    </table>


                  </div>
                  <div className={styles.msGridcol12}>
                    <label className={styles.label}>(<span>*</span>) Reasons why a budgeted activity has been dropped (if it is no longer needed or can be postponed to the following year)</label>
                  </div>

                </div>
              </div>
            </div>
          </div>

          <div className={styles.msGrid}>
            <div className={styles.msGridrow}>
              <div className={styles.msGridcol12}>
                <DefaultButton text="Cancel" className={styles.btn_red} onClick={handleResetForm} />
                <DefaultButton text="Send" className={styles.btn_blue} onClick={handleSend} disabled={this.state.isDisableBtn} />
                <DefaultButton text="Save draft" className={styles.btn_blue} onClick={handleSaveDraft} disabled={this.state.isDisableBtn} />
              </div>
              <div className={styles.msGridcol12}>
                <div className={styles.separador}></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );

  }

}

function _onChange(ev: React.FormEvent<HTMLInputElement>, option: IChoiceGroupOption): void {
  console.dir(option);
}


