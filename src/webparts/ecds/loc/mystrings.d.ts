declare interface IEcdsWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'EcdsWebPartStrings' {
  const strings: IEcdsWebPartStrings;
  export = strings;
}
