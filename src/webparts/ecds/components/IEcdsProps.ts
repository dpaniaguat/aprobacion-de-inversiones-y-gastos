export interface IEcdsProps {
  checked: { color: string; };
  description: string;
  multiValueOptions: any;
  singleValueOptions: any;
}
