import * as React from 'react';
import styles from './Ecds.module.scss';
import { IEcdsProps } from './IEcdsProps';
import { escape } from '@microsoft/sp-lodash-subset';

import { Guid } from '@microsoft/sp-core-library';
import { sp, Web, IWeb } from "@pnp/sp/presets/all";
import "@pnp/sp/lists";
import "@pnp/sp/items";
import { Dropdown, PrimaryButton, IDropdownOption, TextField, Label, DefaultButton, Link, Panel, Icon, Checkbox, ICheckboxStyles, ICheckboxStyleProps, IButtonStyles } from 'office-ui-fabric-react';

var arr = [];
export interface IDropdownStates {
  singleValueDropdown:string;
  multiValueDropdown:any;
}

export default class Ecds extends React.Component<IEcdsProps, IDropdownStates> {

  constructor(props)
  {
    super(props);
    this.state={
      singleValueDropdown:"",
      multiValueDropdown:[]
    };
  }
  
  public onDropdownChange = (event: React.FormEvent<HTMLDivElement>, item: IDropdownOption): void => {
    this.setState({ singleValueDropdown: item.key as string});
  }

  public onDropdownMultiChange = async (event: React.FormEvent<HTMLDivElement>, item: IDropdownOption): Promise<void> => {
    if (item.selected) {
      arr.push(item.key as string);
    }
    else {
      arr.indexOf(item.key) !== -1 && arr.splice(arr.indexOf(item.key), 1);
    }
    this.setState({ multiValueDropdown: arr });
  }

  public render(): React.ReactElement<IEcdsProps> {
    return (
      <div className={ styles.ecds }>
        <div className={ styles.container }>
          <div className={styles.box}>
            <div className={styles.header}>
              <h3>General Information</h3>
            </div>
            <div className={styles.content}>
              <div className={styles.msGrid}>
                <div className={styles.msGridrow}> 
                  <div className={styles.msGridcol2}>
                    <label className={styles.head}>To:</label>
                    <label className={styles.data}>Mangement Committee</label>
                  </div>
                  <div className={styles.msGridcol2}>
                    <label className={styles.head}>Copy to:</label>
                    <label className={styles.data}>General Manager</label>
                  </div>
                  <div className={styles.msGridcol2}>
                    <label className={styles.head}>Date of inssuance:</label>
                    <label className={styles.data}>15/09/21</label>
                  </div>
                  <div className={styles.msGridcol2}>
                    <div className={styles.textfield}><TextField label="From:" /></div>
                  </div>
                  <div className={styles.msGridcol2}>
                    <label className={styles.head}>Area:</label>
                    <label className={styles.data}>General Manager</label>
                  </div>
                  <div className={styles.msGridcol2}>
                    <label className={styles.head}>Email:</label>
                    <label className={styles.data}>esuarez@correo.com</label>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className={styles.box}>
            <div className={styles.header}>
              <h3>Management Decision Sheet</h3>
            </div>
            <div className={styles.content}>
              <div className={styles.msGrid}>
                <div className={styles.msGridrow}> 
                  <div className={styles.msGridcol4}>
                    <label className={styles.head}>Type:</label>
                    <div className={styles.msGridcol4}>
                      <div className={styles.checkbox}><Checkbox label="OPEX" defaultChecked/></div>
                    </div>
                    <div className={styles.msGridcol4}>
                      <div className={styles.checkbox}><Checkbox label="CAPEX" /></div>
                    </div>
                    <div className={styles.msGridcol4}>
                      <div className={styles.checkbox}><Checkbox label="Others" /></div>
                    </div>
                  </div>
                  <div className={styles.msGridcol8}>
                    <div className={styles.textfield}><TextField label="Subject:" /></div>
                  </div>                  
                </div>
                <div className={styles.msGridrow}> 
                  <div className={styles.msGridcol4}>
                    <div className={styles.textfield}><TextField label="Company:" /></div>
                  </div>
                </div> 
              </div>
            </div>
          </div>

          <div className={styles.box}>
            <div className={styles.header}>
              <h3>Proposed Decision and Explanation (if additional information: attachment)</h3>
            </div>
            <div className={styles.content}>
              <div className={styles.msGrid}>

              <div className={styles.msGridrow}> 
                  <div className={styles.msGridcol12}>
                    <label className={styles.head}>1. Proposed decision <span>*</span></label>
                    <div className={styles.textfield}><TextField multiline rows={3} /></div>
                  </div>
                </div>

                <div className={styles.msGridrow}> 
                  <div className={styles.msGridcol12}>
                    <label className={styles.head}>2. Measures</label>
                    <div className={styles.textfield}><TextField multiline rows={3} /></div>
                  </div>
                </div>

                <div className={styles.msGridrow}> 
                  <div className={styles.msGridcol12}>
                    <label className={styles.head}>3. Technical and Economic Justification and Financing<span>*</span></label>
                    <div className={styles.textfield}><TextField multiline rows={3} /></div>
                  </div>
                </div>

                <div className={styles.msGridrow}> 
                  <div className={styles.msGridcol12}>
                    <label className={styles.head}>4. Effects on service quality, capacity, personal Opex and/or Capex</label>
                    <div className={styles.textfield}><TextField multiline rows={3} /></div>
                  </div>
                </div>

                <div className={styles.msGridrow}> 
                  <div className={styles.msGridcol12}>
                    <label className={styles.head}>5. Accordance with approved budget and/or medium term plans</label>
                    <div className={styles.textfield}><TextField multiline rows={3} /></div>
                  </div>
                </div>

                <div className={styles.msGridrow}> 
                  <div className={styles.msGridcol12}>
                    <label className={styles.head}>6. Alternatives</label>
                    <div className={styles.textfield}><TextField multiline rows={3} /></div>
                  </div>
                </div>

                <div className={styles.msGridrow}> 
                  <div className={styles.msGridcol9}>
                    <label className={styles.head}>6. Alternatives</label>
                  </div>
                  <div className={styles.msGridcol3}>
                    <label className={styles.adjunto}><Icon iconName={"FabricOpenFolderHorizontal"} /><a href="#">Open attachments folder</a></label>
                  </div>
                </div> 

                <div className={styles.msGridrow}> 
                  <div className={styles.msGridcol12}>
                    Tabla
                  </div>                  
                </div> 


              </div>
            </div>
          </div>

          <div className={styles.msGrid}>
            <div className={styles.msGridrow}>
              <div className={styles.msGridcol6}>
                <label className={styles.label}>(<span>*</span>) Mandatory field</label>
              </div>              
              <div className={styles.msGridcol6}>
                <DefaultButton text="Cancel" className={styles.btn_red} />
                <DefaultButton text="Send" className={styles.btn_blue} />
                <DefaultButton text="Save draft" className={styles.btn_blue} />
              </div>
              <div className={styles.msGridcol12}>
                <div className={styles.separador}></div>
              </div>              
            </div>
          </div>



        </div>
      </div>
    );

  }
}
